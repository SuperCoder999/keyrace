import express, { Express } from "express";
import router from "./router";
import socketIO, { Server as IOServer } from "socket.io";
import { Server as HTTPServer } from "http";
import { join as joinPath } from "path";
import socketHandler from "./socket";

const server: Express = express();
const socketServer: HTTPServer = new HTTPServer(server);
const io: IOServer = socketIO(socketServer);

server.use(express.static(joinPath(__dirname, "../../", "public", "css")));
server.use(express.static(joinPath(__dirname, "../../", "public", "audio")));
server.use(express.static(joinPath(__dirname, "../", "client")));

server.get("/config.js", (req, res) => {
    res.sendFile(joinPath(__dirname, "../", "config.js"))
});

socketHandler(io);
router(server);

socketServer.listen(process.env.PORT || 3002, () => {
    console.log("Server is listening on port 3002");
});
