import { Socket, Server as IOServer } from "socket.io";
import { Player, minimizePlayer } from "../types/player";
import * as config from "../data/config";
import { playerCondition } from "../helpers/player";
import { sendNewPlayers } from "../helpers/player";
import { RoomInfo } from "../types/room";
import { sendNewRooms } from "../helpers/room";
import { onLeave } from "./room.leave";

export default (socket: Socket, io: IOServer, connected: Player[], roomsMap: Map<string, RoomInfo>): void => {
    socket.on("GET_PLAYERS", (): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;
        sendNewPlayers(<string>player.room, connected);
    });

    socket.on("GET_ROOMS", (): void => {
        sendNewRooms(io, roomsMap);
    });

    socket.on("SET_READY_STATUS", (isReady: boolean): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        const index: number = connected.findIndex(playerCondition(socket));
        if (!player.room) return;
        connected[index].ready = isReady;
        sendNewPlayers(<string>player.room, connected);
    });

    socket.on("START_PRE_GAME_TIMER", () => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        
        if (!room.playing) {
            roomsMap.set(<string>player.room, <RoomInfo>{ ...room, playing: true });
            let time = config.SECONDS_TIMER_BEFORE_START_GAME;

            sendNewRooms(io, roomsMap);

            const interval = setInterval((): void => {
                io.to(<string>player.room).emit("SET_PRE_GAME_TIMER", time);
                time--;

                if (time === -1) {
                    io.to(<string>player.room).emit("FINISH_PRE_GAME_TIMER");
                    clearInterval(interval);
                }
            }, 1000);
        }
    });

    socket.on("CHOOSE_TEXT", (text: string): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        
        if (!room.textChoosen) {
            roomsMap.set(<string>player.room, <RoomInfo>{
                ...room,
                textChoosen: true,
                text
            });
        }
    });

    socket.on("START_GAME_TIMER", () => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);
        
        if (!room.mainTimerStarted) {
            roomsMap.set(<string>player.room, <RoomInfo>{ ...room, mainTimerStarted: true });
            let time = config.SECONDS_FOR_GAME;
            io.to(<string>player.room).emit("SET_TEXT", room.text);

            const interval = setInterval((): void => {
                io.to(<string>player.room).emit("SET_GAME_TIMER", time);
                time--;

                if (time === -1) {
                    const rating = teammates.sort((p0, p1) => p0.progress < p1.progress ? 1 : -1).map(minimizePlayer);
                    onFinish(teammates, room, <string>player.room);
                    io.to(<string>player.room).emit("FINISH_GAME", rating);
                    clearInterval(interval);
                }
            }, 1000);

            socket.on("KILL_GAME_TIMER", (): void => {
                const player: Player = <Player>connected.find(playerCondition(socket));
                if (!player.room) return;
                clearInterval(interval);
            });
        }
    });

    function onFinish(players: Player[], room: RoomInfo, roomName: string): void {
        for (const player of players) {
            const index: number = connected.findIndex((sPlayer: Player): boolean => player.name === sPlayer.name);
            connected[index].ready = false;
            connected[index].progress = 0;
        }

        roomsMap.set(roomName, <RoomInfo>{ ...room, text: null, textChoosen: false, playing: false, rating: [], mainTimerStarted: false });
        sendNewRooms(io, roomsMap);
        sendNewPlayers(roomName, connected);
    }

    socket.on("SET_PROGRESS", (progress: number): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        const index: number = connected.findIndex(playerCondition(socket));
        if (!player.room) return;

        let room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);

        connected[index].progress = progress;
        sendNewPlayers(player.room, connected);

        if (progress >= 100) {
            roomsMap.set(<string>player.room, { ...room, rating: [...room.rating, minimizePlayer(player)] });
        }

        room = <RoomInfo>roomsMap.get(<string>player.room);

        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);

        if (room.rating.length >= teammates.length) {
            onFinish(teammates, room, player.room);
            io.to(<string>player.room).emit("FINISH_GAME", room.rating);
        }
    });

    socket.on("COMPLETE_LEAVE_ROOM", () => {
        const player: Player | undefined = connected.find(playerCondition(socket));
        if (!player || !player.room) return;

        const room = <RoomInfo>roomsMap.get(<string>player.room);

        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);

        if (room.rating.length >= teammates.length - 1 && room.rating.length !== 0) {
            onFinish(teammates, room, player.room);
            io.to(<string>player.room).emit("FINISH_GAME", room.rating);
        }

        onLeave(io, socket, connected, roomsMap);
    });
};
