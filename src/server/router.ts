import { Express, Request, Response } from "express";
import { join as joinPath } from "path";
import { texts } from "./data/data";

export default (server: Express): void => {
    server.get("/login", (req: Request, res: Response): void => {
        res.sendFile(joinPath(__dirname, "../../", "public", "html", "login.html"));
    });

    server.get("/game", (req: Request, res: Response): void => {
        res.sendFile(joinPath(__dirname, "../../", "public", "html", "game.html"));
    });

    server.get("/game/texts/:id", (req: Request, res: Response): void => {
        const id = Number(req.params.id);

        if (isNaN(id)) {
            return void res.status(400).json({ status: 400, message: "Unexpected id - " + req.params.id + ": not a number" });
        }

        if (id >= texts.length) {
            return void res.status(400).json({ status: 400, message: "Unexpected id - " + req.params.id + ": index is out of range" });
        }

        res.json({ data: texts[id] })
    });

    server.get("*", (req: Request, res: Response): void => {
        res.redirect("/login");
    });
};
