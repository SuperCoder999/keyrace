import { PlayerMin } from "./player";

export interface RoomInfo {
    count: number;
    playing: boolean;
    rating: PlayerMin[];
    textChoosen: boolean;
    text: string | null;
    mainTimerStarted: boolean;
}
