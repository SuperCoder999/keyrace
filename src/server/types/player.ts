import { Socket } from "socket.io";

export interface Player {
    name: string,
    socket: Socket,
    room: string | null,
    ready: boolean,
    progress: number;
}

export interface PlayerMin {
    name: string,
    room: string | null,
    ready: boolean,
    progress: number
}

export function minimizePlayer(player: Player): PlayerMin {
    return {
        name: player.name,
        room: player.room,
        ready: player.ready,
        progress: player.progress
    };
}
